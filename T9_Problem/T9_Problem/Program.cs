﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9_Problem
{
     public class Program
    {
        static void Main(string[] args)
        {

            int NumberOfCases;
            NumberOfCases = int.Parse(Console.ReadLine());
            while (NumberOfCases > 0)
            {
               
                Console.WriteLine("Case # {0}: {1}", NumberOfCases, OutputText(Console.ReadLine()));
                NumberOfCases--;

            }
        }
        public static string OutputText(string input)
        {
            Dictionary<char, int> buttons, KeysInput;
            buttons = new Dictionary<char, int>
            {
                {'a',2 },
                {'b',2 },
                {'c',2 },
                {'d',3 },
                {'e',3 },
                {'f',3 },
                {'g',4},
                {'h',4},
                {'i',4},
                {'j',5 },
                {'k',5 },
                {'l',5 },
                {'m',6 },
                {'n',6 },
                {'o',6 },
                {'p',7 },
                {'q',7 },
                {'r',7 },
                {'s',7 },
                {'t',8 },
                {'u',8 },
                {'v',8 },
                {'w',9 },
                {'x',9 },
                {'y',9 },
                {'z',9 },
                {' ',0 }

            };
            KeysInput = new Dictionary<char, int>
            {
                {'a',2},
                {'b',22 },
                {'c', 222 },
                {'d', 3 },
                {'e', 33 },
                { 'f',333 },
                {'g',4},
                {'h',44},
                {'i',444},
                {'j',5 },
                {'k',55 },
                {'l',555 },
                {'m',6 },
                {'n',66 },
                {'o',666},
                {'p',7 },
                {'q',77 },
                {'r',777 },
                {'s',7777 },
                {'t',8 },
                {'u',88 },
                {'v',888 },
                {'w',9 },
                {'x',99 },
                {'y',999 },
                {'z',9999 },
                { ' ',0}

            };


           
            string output = "";
           
            for (int i = 0; i < input.Length; i++)
            {
                if (i > 0)
                {
                    if (buttons[input[i]] == buttons[input[i - 1]])
                    {
                        output += " ";
                    }
                }
                output += KeysInput[input[i]];
            }
            return output;
        }
    }
}
