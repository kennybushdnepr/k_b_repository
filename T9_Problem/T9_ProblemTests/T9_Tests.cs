﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace T9_ProblemTests
{
    [TestClass]
    public class T9_Tests
    {
        [TestMethod]
        public void CaseOutputForHi()
        {
            //arrange
            string input = "hi";
            //act
            string output=T9_Problem.Program.OutputText(input);
            //assert
            Assert.AreEqual("44 444",output);
        }
        [TestMethod]
        public void CaseOutputForYes()
        {
            //arrange
            string input = "yes";
            //act
            string output = T9_Problem.Program.OutputText(input);
            //assert
            Assert.AreEqual("999337777", output);
        }
        [TestMethod]
        public void CaseOutputFor_hello_world()
        {
            //arrange
            string input = "hello world";
            //act
            string output = T9_Problem.Program.OutputText(input);
            //assert
            Assert.AreEqual("4433555 555666096667775553", output);
        }
        [TestMethod]
        public void CaseOutputForYes__Yes()
        {
            //arrange
            string input = "yes  yes";
            //act
            string output = T9_Problem.Program.OutputText(input);
            //assert
            Assert.AreEqual("9993377770 0999337777", output);
        }
    }
}
